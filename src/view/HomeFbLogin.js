import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading, Asset, Font, Icon } from 'expo';
import { LoginButton } from 'react-native-fbsdk';
import { Button } from 'react-native-elements';
export default class HomeFbLogin extends React.Component {
    constructor(props) {
        super(props);

        
        
    }
    logIn = async () => {
        try {
            const {
                type,
                token,
                expires,
                permissions,
                declinedPermissions,
            } = await Expo.Facebook.logInWithReadPermissionsAsync('<APP_ID>', {
                permissions: ['public_profile'],
            });
            if (type === 'success') {
                // Get the user's name using Facebook's Graph API
                const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
                Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
            } else {
                // type === 'cancel'
            }
        } catch ({ message }) {
            alert(`Facebook Login Error: ${message}`);
        }
    }
    render() {
        return (
            <View style={styles.container}>
             <Button style={styles.tabDetail}></Button>
            </View>
        );

    }
}
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
        backgroundColor: '#fff',
        height : '100%',
        width:'100%'
    },
    tabDetail: {
		position: 'absolute',
		right: '5%',
        top: '60%',
        height: 300

	},
});
